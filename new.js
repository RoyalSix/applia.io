var fs = require('fs-extra');
var args = process.argv.slice(2);
if (args && args.length >= 3) {
  let templateString = require('./template');
  templateString = templateString.replace('username', args[0])
  templateString = templateString.replace('password', args[1])
  var tags = args[2].split(',').map((tag) => tag.trim()).join("','");
  tags += "'";
  tags = "'" + tags;
  templateString = templateString.replace('tags: []', `tags: [${tags}]`)
  if (args[3]) {
    var values = args[3].split(' ');
    templateString = templateString.replace(/like:.*\d+/ig, `like: ${values[0]}`)
    templateString = templateString.replace(/comment:.*\d+/ig, `comment: ${values[1]}`)
    templateString = templateString.replace(/follow:.*\d+/ig, `follow: ${values[2]}`)
  }
  fs.ensureDirSync('./user-run-list')
  fs.writeFileSync(`./user-run-list/${args[0]}.js`, templateString)
} else {
  console.log('wrong number of arguements given');
  console.log(`Format like this 'username' 'password' 'basketball, sports, code, pranks'`)
}