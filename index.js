const { spawn } = require('child_process');
var app = require('express')();
var bodyParser = require('body-parser');
var fs = require('fs-extra');
var cors = require('cors');
var { Instagram, firebase } = require('instagram-api-node');
var runningUsers = {};
var moment = require('moment');

app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded
app.use(cors()); // Cross origin

app.get('/', function (req, res) {
  res.status(200).end('This is the applia endpoint, but this page not a route.');
});

app.post('/new-user', function (req, res) {
  if (req && req.body) {
    var { username, tags, values } = req.body;
    firebase.database().ref(`users/${username}/bot/settings/options/values`).set(values).then(() => {
      firebase.database().ref(`users/${username}/bot/settings/options/tags`).set(tags).then(() => {
        console.log(`Setup bot for user ${username}`);
        res.status(200).end('OK');
      })
    });
  }
});

app.post('/start-user', function (req, res) {
  if (req && req.body) {
    var { username, log } = req.body;
    if (username) {
      const user_bot_running = spawn('node', ['./user-run.js', username]);
      sendOutput(user_bot_running, username, log);
      runningUsers[username] = user_bot_running.pid;
      res.status(200).send('OK');
      console.log(`Starting running bot on user ${username}`);
    } else {
      res.status(400).end('Invalid parameters');
      console.log('wrong number of arguements given');
    }
  }
});

app.post('/stop-user', function (req, res) {
  if (req && req.body) {
    var { username, log } = req.body;
    if (username) {
      let processID = runningUsers[username];
      if (!processID) return res.status(200).end('OK');
      const user_bot_running = spawn('kill', ['-9', processID]);
      sendOutput(user_bot_running, username, log);
      runningUsers[username] = null;
      return res.status(200).end('OK');
      console.log(`Stopped running bot on user ${username}`);
    } else {
      res.status(400).end('Invalid parameters');
      console.log('wrong number of arguements given');
    }
  }
});

app.get('/getAccountOptions', function (req, res) {
  console.log(runningUsers);
  if (req && req.query && req.query.username) {
    var { username } = req.query;
    firebase.database().ref(`users/${username}/bot/settings/options`).once('value').then((snapshot) => {
      let options = snapshot.val() || {};
      options['running'] = !!runningUsers[username]
      return res.status(200).end(JSON.stringify(options));
    });
  } else {
    res.status(400).end('Invalid parameters');
    console.log('wrong number of arguements given');
  }
});

app.get('/getInstagramToken', function (req, res) {
  if (req && req.query) {
    var { username, password } = req.query;
    if (username && password) {
      let api = new Instagram(username, password);
      api.getAccessTokenFromLogin().then((token) => {
        if (token)
          res.status(200).send(token);
        else
          //TODO: Add better error handling
          res.status(400).send('You entered the wrong password or username');
      });
    } else {
      res.status(400).end('Invalid parameters');
      console.log('wrong number of arguements given');
    }
  }
});


function sendOutput(childProcess, username, log) {
  childProcess.stdout.on('data', (data) => {
    fs.ensureDirSync('./log/');
    var exec = require('child_process').exec;
    let output = moment().format("dddd, MMMM Do YYYY, h:mm:ss a") + " " + data.toString();
    let filepath = `./log/${username}_log.txt`;
    exec(`echo "${output}" >> ${filepath}`)
  });

  childProcess.stderr.on('data', (data) => {
    fs.ensureDirSync('./log/');
    var exec = require('child_process').exec;
    let output = moment().format("dddd, MMMM Do YYYY, h:mm:ss a") + " " + data.toString();
    let filepath = `./log/${username}_log.txt`;
    exec(`echo "${output}" >> ${filepath}`);
    console.log(data.toString());
  });

  childProcess.on('close', (code) => {
    console.log(`user ${username} exited with code ${code}`);
    runningUsers[username] = null;
    console.log(`Stopped running bot on user ${username}`);
  });
}

var server = app.listen(8080);

process.on('SIGINT', function () {
  server.close(() => {
    for (var username in runningUsers) {
      const user_bot_running = spawn('kill', ['-9', runningUsers[username]]);
      sendOutput(user_bot_running, username);
    }
    console.log('Ended all users');
    process.exit(0);
  })
})