var args = process.argv.slice(2);
if (args && args.length === 1) {
  var { Bot, Instagram } = require('instagram-api-node');
  var username = args[0];
  var api = new Instagram(username);

  (async function () {
    api.login().then(() => {
      var bot = new Bot(api).start()
    })
  })()
} else {
  console.log('wrong number of arguements given');
}